const newID = require('./newid')

class Artist{
    constructor(name, country){
        this.id = newID();
        this.albums = [];
        this.name = name;
        this.country = country;
    }

    existAlbum(albumName){
        return this.albums.find(album => album.name === albumName)
    }

    addAlbum(album){
        this.albums.push(album);
    }

    getTracks(){
        let tracks = this.albums.map(album => album.tracks).reduce(function(a,b){
            return a.concat(b)
          },[])
          return tracks
    }

    printData(){
        console.log(JSON.stringify(this));
    }
}

module.exports = Artist