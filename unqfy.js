const Album = require('./album.js')
const Artist = require('./artist.js')
const Track = require('./track.js')
const Playlist = require('./playlist.js')
const ArtistExistError = require('./artistExistError.js')
const TrackExistInAlbumError = require('./trackExistInAlbumError')
const picklify = require('picklify'); // para cargar/guarfar unqfy
const fs = require('fs'); // para cargar/guarfar unqfy


class UNQfy {
  
  constructor(){
    this.playlists = [];
    this.artists = [];
  }
  
  addArtist(artistData) {
    if(!this.existArtist(artistData.name)){
      let artist = new Artist(artistData.name, artistData.country);
      this.artists.push(artist);
      console.log('Se agrego el artista ' + artistData.name + ' correctamente.');
      return artist;
    }else {
      throw new ArtistExistError()
    }
  }

  addAlbum(artistName, albumData) {
    let artist = this.getArtistByName(artistName)
    if(artist !== undefined){
      if(!artist.existAlbum(albumData.name)){
        let album = new Album(artist.id,  albumData.name, albumData.year);
        artist.addAlbum(album);
        console.log('Se agrego el album '+ albumData.name + ' correctamente.');
        return album;
      }else{
        console.log('No se pudo agregar el album '+ albumData.name + ' debido a que ya existe.');
      }
    }else{
      throw new ArtistExistError()
    }
  }

  addTrack(albumName, trackData) {
    let album = this.getAlbumByName(albumName);
    if(album !== undefined){
      if(!album.hasTrack(trackData.name)){
        let track = new Track(trackData.name, trackData.duration, trackData.genres, album.id);
        album.addTrack(track);
        console.log('Se agrego el track '+ trackData.name + ' correctamente.');
        return track;
      }else{
        throw new TrackExistInAlbumError()
      }
    }
  }

  getTracksMatchingGenres(genres) {
    let allTracks = this.getAllTracks()
    let tracks = allTracks.filter(track => track.hasASomeGenres(genres))
    return tracks 
  }

  getTracksMatchingArtist(artistName) {
    let artistTracks = []
    let artist = this.artists.find(artist => artist.name === artistName)
    if(artist !== undefined){
      artistTracks = artist.getTracks()
    }else{
      throw Error ('El artista ' + artistName.name + ' no existe.');
    }
    return artistTracks
  }

  getTracksByAlbum(albumName) {
    let album = this.getAlbumByName(albumName)
    if (album !== undefined) {
  
      let trackList = album.tracks;
      return trackList;
    } else {
         throw Error ("No se encontro un album con el nombre " + albumName);
    }
  }

  createPlaylist(name, genresToInclude, maxDuration) {
    let playlist = new Playlist(name, genresToInclude, maxDuration)
    let tracks = this.getTracksMatchingGenres(genresToInclude)
    tracks.forEach(track => playlist.addTrack(track));
    this.playlists.push(playlist);
    console.log('Se ha agregado la playlist' + name + ' correctamente.')
    return playlist
  }

  removeArtist(artistName){
    let artist = this.getArtistByName(artistName)
    if(artist !== undefined){
      this.artists = this.artists.filter(art => art.name !== artist.name)
      this.playlists.map(playlist => playlist.removeArtistAlbums(artist))
      console.log("Se ha eliminado el artista " + artist.name + " correctamente")
    }else{
      throw Error("No se pudo eliminar el artista " + artistName +" debido a que no existe")
    }
  }

  removeAlbum(artistName,albumName){
    let artist = this.getArtistByName(artistName)
    if ( artist !== undefined) {
      let album = artist.albums.find(album => album.name === albumName)
      if ( album !== undefined) {
        artist.albums = artist.albums.filter(album => album.name !== albumName)
        this.playlists.map(playlist => playlist.removeAllTracksAlbum(album))
        console.log("Se ha eliminado el album " + albumName + " del artista " + artistName + " correctamente")
      }else {
        throw Error('No se pudo eliminar el album ' + albumName + ' debido a que no existe ')
      }
    }else {
      throw Error('No se pudo eliminar el album ' + albumName + ' debido a que no existe el artista ' + artistName)
    }
  }

  removeTrack(artistName, trackName){
    let artist = this.getArtistByName(artistName)
    if(artist !== undefined){
      let album = artist.albums.find(album => album.hasTrack(trackName))
      if(album !== undefined){
        album.removeTrack(trackName)
        this.playlists.map(playlist => playlist.removeTrack(trackName))
        console.log('Se ha eliminado el track ' + trackName + ' correctamente');
      }else{
        throw Error('El track que ha ingresado no existe.');
      }
    }
  }

  removePlaylist(id){
    let playlist = this.getPlaylistById(id);
    if(playlist !== undefined){
      this.playlists = this.playlists.filter(playlist => playlist.id !== id)
      console.log('Se ha eliminado la playlist' + playlist.name + ' correctamente');
    }else{
      throw Error('La playlist que ha ingresado no existe.');
    }
  }

  getArtistByName(name) {
    let artist = this.artists.find(artist => artist.name === name)
    if(artist !== undefined){
      return artist
    }
    else{
      throw Error("No se encontro al artista " + name)
    }
  }

  getArtistById(id) {
    let artist = this.artists.find(artist => artist.id === id)
    if(artist !== undefined){
      return artist
    }
    else{
      throw Error("No se encontro un artista con el id " + id)
    }
  }

  getAlbumByName(name) {
    let albums = this.getAllAlbums()
    let album = albums.find(album => album.name === name)
    if(album !== undefined){
      return album
    }else{
      throw Error("No se encontro un album con el nombre " + name)
    }
  }

  getAlbumById(id) {
    let albums = this.getAllAlbums()
    let album = albums.find(album => album.id === id)
    if(album !== undefined){
      return album
    }else{
      throw Error("No se encontro un album con ese id ")
    }
  }

  getTrackByName(name) {
    // retorna al track con el id indicado si es que existe
    let allTracks = this.getAllTracks()
    let track = allTracks.find(track => track.name === name)
    if (track !== undefined) {
      return track
    }else{
      throw Error("No se encontro un track con el nombre " + name)
    }
  }

  getTrackById(id) {
    // retorna al track con el id indicado si es que existe
    let allTracks = this.getAllTracks()
    let track = allTracks.find(track => track.id === id)
    if (track !== undefined) {
      return track
    }else{
      throw Error("No se encontro un track con el id " + id)
    }
  }

  getPlaylistByName(name) {
    // retorna la playlist con el id indicado, si es que existe
    let playList = this.playlists.find(playlist => playlist.name == name)
    if ( playList !== undefined) {
      return playList
    }else {
      throw Error("No se encontro una playList con el nombre " + name)
    }
  }

  getPlaylistById(id) {
    // retorna la playlist con el id indicado, si es que existe
    let playList = this.playlists.find(playlist => playlist.ID == id)
    if ( playList !== undefined) {
      return playList
    }else {
      throw Error("No se encontro una playList con el id " + id)
    }
  }

  getAllArtists(){
    return this.artists.reduce(function(a,b){
      return a.concat(b);
    },[])
  }

  getAllAlbums(){
    let allAlbums = []
    if(this.artists.length !== 0){
      allAlbums = this.artists.map(artist => artist.albums).reduce(function(a,b){
        return a.concat(b);
      },[])
    }
    return allAlbums;
  }

  getAllTracks(){
    let allTracks = this.getAllAlbums().map(album => album.tracks).reduce(function(a,b){
      return a.concat(b)
    },[])
    return allTracks
  }

  getAllPlaylists(){
    let res = []
    this.playlists.forEach(playlist => res.push(playlist.name,playlist.duration(),this.names(playlist.tracks)))
    return res
 }

  getAlbumsByArtistName(artistName){
    let artist = this.getArtistByName(artistName)
    if (artist !== undefined) {
            let albumsList = artist.albums
            return albumsList
        } else {
            throw Error("El artista " + artistName + ' no existe.')
        }
  }
  
  names(items){
    return items.map(item => item.name)
  }

  searchByName(name){
    let tracks = this.searchTracksByName(name)
    let albums =this.searchAlbumsByName(name)
    let artists = this.searchArtistsByName(name)
    let playlists = this.searchPlaylistByName(name)
    console.log('Artistas:' + this.names(artists),
                'Albums:' + this.names(albums),
                'Tracks:' + this.names(tracks),
                'Playlists:' + this.names(playlists));
    return {artists,albums,tracks,playlists}
  }

  searchTracksByName(name){
    // retorna todos los tracks que tengan incluido el nombre indicado
    let tracks = this.getAllTracks()
    return tracks.filter(track => track.name.includes(name))
  }

  searchAlbumsByName(name){
    // retorna todos los albums que tengan incluido el nombre indicado
    let albums = this.getAllAlbums()
    return albums.filter(album => album.name.toLowerCase().includes(name.toLowerCase()))
  }

  searchPlaylistByName(name){
    // retorna todas los playlists que tengan incluido el nombre indicado
    return this.playlists.filter(playlist => playlist.name.includes(name))
  }

  searchArtistsByName(name){
    // retorna todos los artistas que tengan incluido el nombre indicado
    return this.artists.filter(artist => artist.name.toLowerCase().includes(name.toLowerCase()))
  }

  searchTracksByName(name){
    let tracks = this.getAllTracks()
    return tracks.filter(track => track.name.includes(name))
  }
  
  existArtist(artistName){
    return this.artists.find(artist => artist.name === artistName);
  }

  save (filename) {
    const serializedData = picklify.picklify(this);
    fs.writeFileSync(filename, JSON.stringify(serializedData, null, 2));
  }

  static load(filename) {
    const serializedData = fs.readFileSync(filename, {encoding: 'utf-8'});
    //COMPLETAR POR EL ALUMNO: Agregar a la lista todas las clases que necesitan ser instanciadas
    const classes = [UNQfy,Artist,Playlist,Track,Album];
    return picklify.unpicklify(JSON.parse(serializedData), classes);
  }
}

// COMPLETAR POR EL ALUMNO: exportar todas las clases que necesiten ser utilizadas desde un modulo cliente
module.exports = {
  UNQfy: UNQfy,
  Artist: Artist,
  Playlist: Playlist,
  Track: Track,
  Album: Album,
  ArtistExistError: ArtistExistError,
  TrackExistInAlbumError: TrackExistInAlbumError  
};

