class ArtistExistError extends Error{
    constructor(){
        super("El artista existe en el sistema")
        this.name = "ArtistExistError"
    }
}

module.exports = ArtistExistError