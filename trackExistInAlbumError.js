class TrackExistInAlbumError extends Error{
    constructor(){
        super("El track ya se encuentra en el album")
        this.name = "trackExistInAlbumError"
    }
}

module.exports = TrackExistInAlbumError