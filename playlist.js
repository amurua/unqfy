const newID = require('./newid')

class Playlist{
    constructor(name, genres, maxDuration){
        this.id = newID();
        this.name = name;
        this.tracks = [];
        this.genres = genres;
        this.maxDuration = maxDuration;
        this.currentDuration = 0;
    }

    hasTrack(track){
        return this.tracks.includes(track);
    }
    
    addTrack(track){
        if(!this.hasTrack(track)){
            let duration = this.duration() + track.duration
            if(duration <= this.maxDuration){
                this.tracks.push(track);
                this.currentDuration = duration;
            }else{
                console.log('No se pudo agregar el track ' + track.name + ' debido a que supera el limite de duración de la playlist.')
            }
        }else{
            console.log('El track ' + track.name + ' ya existe en esta playlist.')
        }
    }

    removeAllTracksAlbum(album) {
        this.tracks = this.tracks.filter(track => track.albumId !== album.albumId)
     }
   
     removeArtistAlbums(artist){
        artist.albums.forEach(album => this.removeAllTracksAlbum(album))
      }

    duration(){
        return this.currentDuration;
    }

    printData(){
        console.log(JSON.stringify(this));
    }
}

module.exports = Playlist