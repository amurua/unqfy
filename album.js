const newID = require('./newid')

class Album{

    constructor(artistId, name, year){
        this.id = newID();
        this.artistId = artistId;
        this.tracks = [];
        this.name = name;
        this.year = year;
    }

    addTrack(trackName){
        if(!this.tracks.includes(trackName)){
          this.tracks.push(trackName)
        } else{
            throw Error("El album ya tiene ese track")
        }
    }

    removeTrack(trackName){
        this.tracks = this.tracks.filter(track => track.name !== trackName)
    }

    hasTrack(trackName){
        return this.tracks.find(track => track.name === trackName);
    }

    printData(){
        console.log(JSON.stringify(this));
    }
}

module.exports = Album