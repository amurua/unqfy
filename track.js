const newID = require('./newid')

class Track{
    constructor(name, duration, genres, albumId){
        this.id = newID();
        this.albumId = albumId;
        this.name = name;
        this.duration = duration;
        this.genres = genres;
    }

    hasASomeGenres(genresNames){
        return genresNames.some(genre => this.genres.indexOf(genre) >= 0)
    }

    printData(){
        console.log(JSON.stringify(this));
    }
}

module.exports = Track